#include<stdio.h>
#include<math.h>
int main()
{
    int degree, result;
    printf("Enter the degree of angle:");
    scanf("%d",&degree); //45
    result = (sin(degree)*sin(degree) + cos(degree)*cos(degree)); // (sin(45)*sin(45) + cos(45)*cos(45))
    printf("Result is: %d",result); // 1
    return 0;
}