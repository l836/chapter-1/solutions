/* Convert Cartesian Co-ordinate to Polar co-ordinates*/
#include<stdio.h>
#include<math.h>
int main(void)
{
  float x,y,r,theta;
  printf("Enter x and y co-ordinates :");
  scanf("%f %f",&x, &y);
  r = sqrt(x*x + y*y);
  theta = atan2(y,x);
  theta = theta * 180/3.14;
  printf("r=%f theta = %f\n",r,theta);
    return 0;
}