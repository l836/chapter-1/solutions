/* Calculate Ramesh Gross Salary*/
#include<stdio.h>
int main(void)
{
  float bs, da, hra, gs;
  printf("Enter Basic Salary of Ramesh\n");
  scanf("%f", &bs);
  da = 0.4 * bs;
  hra = 0.2 * bs;
  gs = bs + da + hra;
  printf("Basic Salary of Ramesh = %f\n", bs);
  printf("Dearness Allowance of Ramesh = %f\n", da);
  printf("House Rate Allowance of Ramesh = %f\n", hra);
  printf("Gross Salary of Ramesh = %f\n", gs);
    return 0;
}