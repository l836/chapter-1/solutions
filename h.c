/* Reverse of digits of a 5 digit number */
#include<stdio.h>
int main(void)
{
  int b, a, n;
  long int revnum = 0; /* Intialise  to zero otherwise it will contain a garbage value*/
  printf("Enter a 5 digit number(less than 32767):");
  scanf("%d", &n);
  a = n % 10; /* last digit extracted as a remainder*/
  n = n/10; /* remaining digits*/
  revnum = revnum + a * 10000L; /* revnum updated with addition of extracted digits*/
  a = n % 10; /* 4th digit*/
  n = n / 10;
  revnum = revnum + a * 1000;
  a = n % 10; /* 3rd digit*/
  n = n / 10;
  revnum = revnum + a * 100;
  a = n % 10; /* 2nd digit*/
  n = n / 10;
  revnum = revnum + a * 10;
  a = n % 10; /* 1st digit*/
   revnum = revnum + a ;
  printf("The reversed number is %ld\n", revnum);
   return 0;
}