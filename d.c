/* Conversion of a temperature from fahrenheit to Centigrade */
#include<stdio.h>
int main(void)
{
  float fr, cent;
  printf("\n Enter The Temperature in fahrenheit: ");
  scanf("%f", &fr);
  cent = 5.0/9.0 * (fr - 32);
  printf("Temperature in Centigrade = %f\n", cent);
    return 0;
}