#include<stdio.h>
int main ()
{
    int num,sum,a,b,c,d,d1,d2,d3,d4;
    printf("Enter the Four digit number:");
    scanf("%d",&num);
    a=num/10;  // 1234/10 = 123.4 = 123
    d4=num%10; // 1234%10 = 4
    b=a/10;    // 123 / 10 = 12.3 = 12
    d3=a%10;   // 123 % 10 = 3
    c=b/10;   //  12 / 10 = 1.2 = 1
    d2=b%10;  // 12 % 10 = 2
    d=c/10;   // 1 / 10 = 0.1 = 0
    d1=c%10;  // 1 % 10 = 1
    sum=d1+d4; // 1+4
    printf("sum of 1st and 4th digit = %d",sum);
    return 0;
}