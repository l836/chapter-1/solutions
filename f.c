/* Interchanging of contents of two variables c & d */
#include<stdio.h>
int main(void)
{
  int c,d,e;
  printf("Enter the number of Locations C:\n");
  scanf("%d", &c);
  printf("Enter the number of Locations D:\n");
  scanf("%d", &d);
  /* Interchanging of contents of two variables c & d  using third variable e as temporary store*/
  e = c;
  c = d;
  d = e;
  printf("New Number at location C = %d\n",c);
  printf("New Number at location D = %d\n",d);
    return 0;
}