/* Calculation of perimeter & area of rectangle and circle */
#include<stdio.h>
int main(void)
{
   int l, b, r, area1, perimeter;
   float area2, circum;
   printf("Enter Length & Breadth of Rectangle :\n");
   scanf("%d %d", &l, &b);
   area1 = l * b;
   perimeter = 2 * (l + b);
   printf ("Area of Rectangle= %d\n", area1);
   printf ("Perimeter of Rectangle = %d\n", perimeter);
   printf ("Enter Radius of Circle : \n");
   scanf("%d", &r);
   area2 = 3.14 * r * r;
   circum = 2 * 3.14 * r;
   printf("Area of circle = %f\n", area2);
   printf("Circumference of circle = %f\n", circum);
    return 0;
}