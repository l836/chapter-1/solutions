/* Sum of digits of a 5 digit number */
#include<stdio.h>
int main(void)
{
  int num, a, n;
  int sum = 0; /* Intialise  to zero otherwise it will contain a garbage value*/
  printf("Enter a 5 digit number(less than 32767):");
  scanf("%d", &num);
  a = num % 10; /* last digit extracted as a remainder*/
  n = num/10; /* remaining digits*/
  sum = sum + a; /* Sum updated with addition of extracted digits*/
  a = n % 10; /* 4th digit*/
  n = n / 10;
  sum = sum + a;
  a = n % 10; /* 3rd digit*/
  n = n / 10;
  sum = sum + a;
  a = n % 10; /* 2nd digit*/
  n = n / 10;
  sum = sum + a;
  a = n % 10; /* 1st digit*/
  sum = sum + a;
  printf("The sum of the 5 dogits of %d is %d\n", num, sum);
   return 0;
}